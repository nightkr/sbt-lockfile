sbtPlugin := true

name := "sbt-lockfile"
organization := "se.nullable.sbtix"
version := "0.1-SNAPSHOT"

publishTo := Some(
  if (isSnapshot.value) {
    Opts.resolver.sonatypeSnapshots
  } else {
    Opts.resolver.sonatypeStaging
  }
)
credentials ++= {
  import scala.util.Properties
  Properties
    .envOrNone("SONATYPE_OSS_USERNAME")
    .map(
      username =>
        Credentials("Sonatype Nexus Repository Manager",
                    "oss.sonatype.org",
                    username,
                    Properties.envOrElse("SONATYPE_OSS_PASSWORD", "")))
}

licenses := Seq("MIT" -> url("https://opensource.org/licenses/MIT"))
homepage := Some(url("https://gitlab.com/teozkr/sbt-lockfile"))

scmInfo := Some(
  ScmInfo(
    url("https://gitlab.com/teozkr/sbt-lockfile"),
    "scm:git@gitlab.com:teozkr/sbt-lockfile.git"
  )
)

developers := List(
  Developer(
    id = "teozkr",
    name = "Teo Klestrup Röijezon",
    email = "teo@nullable.se",
    url = url("https://nullable.se")
  )
)

useGpg := true
pgpPublicRing := Path.userHome / ".gnupg" / "pubring.kbx"
// Secret rings are no more, as of GPG 2.2
// See https://github.com/sbt/sbt-pgp/issues/126
pgpSecretRing := pgpPublicRing.value

addSbtPlugin("io.get-coursier" % "sbt-coursier" % "1.1.0-M5")

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core" % "0.9.3",
  "io.circe" %% "circe-generic" % "0.9.3",
  // SBT ships with an ancient version of Jawn that Circe doesn't like
  // https://github.com/circe/circe/issues/823#issuecomment-355542723
  "io.circe" %% "circe-jackson29" % "0.9.0"
)
addCompilerPlugin(
  "org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full)

scriptedLaunchOpts ++= Seq(
  s"-Dplugin.version=${version.value}"
)
scriptedBufferLog := false

publishMavenStyle := false
publishArtifact in (Compile, packageBin) := true
publishArtifact in (Test, packageBin) := false
publishArtifact in (Compile, packageDoc) := false
publishArtifact in (Compile, packageSrc) := false

scalafmtOnCompile := true
