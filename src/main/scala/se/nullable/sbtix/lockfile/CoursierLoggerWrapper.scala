package se.nullable.sbtix.lockfile

import coursier.{Cache, FileError}
import java.io.File

class CoursierLoggerWrapper(innerLogger: Cache.Logger) extends Cache.Logger {
  override def foundLocally(url: String, f: File): Unit = {
    innerLogger.foundLocally(url, f)
  }
  override def downloadingArtifact(url: String, file: File): Unit = {
    innerLogger.downloadingArtifact(url, file)
  }
  override def downloadedArtifact(url: String, success: Boolean): Unit = {
    innerLogger.downloadedArtifact(url, success)
  }

  override def downloadProgress(url: String, downloaded: Long): Unit =
    innerLogger.downloadProgress(url, downloaded)

  override def checkingUpdates(url: String,
                               currentTimeOpt: Option[Long]): Unit =
    innerLogger.checkingUpdates(url, currentTimeOpt)
  override def checkingUpdatesResult(url: String,
                                     currentTimeOpt: Option[Long],
                                     remoteTimeOpt: Option[Long]): Unit =
    innerLogger.checkingUpdatesResult(url, currentTimeOpt, remoteTimeOpt)

  override def downloadLength(url: String,
                              totalLength: Long,
                              alreadyDownloaded: Long,
                              watching: Boolean): Unit =
    innerLogger.downloadLength(url, totalLength, alreadyDownloaded, watching)

  override def gettingLength(url: String): Unit =
    innerLogger.gettingLength(url)
  override def gettingLengthResult(url: String, length: Option[Long]): Unit =
    innerLogger.gettingLengthResult(url, length)

  override def removedCorruptFile(url: String,
                                  file: File,
                                  reason: Option[FileError]): Unit =
    innerLogger.removedCorruptFile(url, file, reason)

  /***
    *
    * @param beforeOutput: called before any output is printed, iff something else is outputed.
    *                      (That is, if that `Logger` doesn't print any progress,
    *                      `initialMessage` won't be printed either.)
    */
  override def init(beforeOutput: => Unit): Unit =
    innerLogger.init(beforeOutput)

  /**
    *
    * @return whether any message was printed by `Logger`
    */
  override def stopDidPrintSomething(): Boolean =
    innerLogger.stopDidPrintSomething()
}
