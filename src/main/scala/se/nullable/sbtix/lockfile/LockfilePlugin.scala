package se.nullable.sbtix.lockfile

import coursier.{CoursierPlugin, Keys => CoursierKeys, SbtLockfileCacheBuster}
import io.circe.syntax._
import sbt.{io => _, _}
import sbt.Keys._

object LockfilePlugin extends AutoPlugin {
  import autoImport._

  override def requires = super.requires && CoursierPlugin

  object autoImport {
    val lockfile =
      taskKey[Option[Lockfile]]("the currently activated lockfile (if any)")
    val lockfilePath =
      settingKey[File]("location of the sbt-lockfile, run freeze to generate")
    val lockfileFrozen = taskKey[Boolean]("whether the lockfile is active")
    val lockfileRequireFrozen = taskKey[Unit](
      "require that the lockfile is active (useful for CI builds)")
  }

  lazy val freezeCommand = Command.command("freeze") { initState =>
    var state = initState
    val extracted = Project.extract(state)
    val lockBuilder = new LockfileBuilder()
    SbtLockfileCacheBuster.clearCoursierUpdateCache()
    for (project <- extracted.structure.allProjectRefs) {
      println(project)
      val (_state, coursierCreateLogger) =
        extracted.runTask(CoursierKeys.coursierCreateLogger, state)
      state = _state
      val cache = extracted.get(CoursierKeys.coursierCache)
      state = extracted.appendWithSession(
        Seq(CoursierKeys.coursierCreateLogger := (
            () => lockBuilder.logger(cache.toPath(), coursierCreateLogger()))),
        state)
      state = extracted.runTask(update, state)._1
    }
    val lockfilePathValue = extracted.get(lockfilePath)
    IO.write(lockfilePathValue, lockBuilder.build.asJson.spaces2)
    initState
  }

  override def projectSettings = Seq(
    commands += freezeCommand,
    lockfilePath := baseDirectory.value / "sbt.lock",
    lockfile := {
      val lockfilePathValue = lockfilePath.value
      if (lockfilePathValue.exists()) {
        val lockfileStr = IO.read(lockfilePathValue)
        Some(
          io.circe.jackson.parse(lockfileStr).flatMap(_.as[Lockfile]).right.get)
      } else {
        None
      }
    },
    lockfileFrozen := lockfilePath.value.exists(),
    lockfileRequireFrozen := {
      val lockfilePathValue = lockfilePath.value
      if (!lockfileFrozen.value) {
        throw new Exception(s"The lockfile $lockfilePathValue does not exist")
      }
    },
    CoursierKeys.coursierCreateLogger := {
      val inner = CoursierKeys.coursierCreateLogger.value
      val cache = CoursierKeys.coursierCache.value
      val lockfileValue = lockfile.value
      () =>
        lockfileValue
          .map(new LockfileVerifier(_))
          .map(_.logger(cache.toPath(), inner()))
          .getOrElse(inner())
    }
  )
}
