package coursier

// The caches are package-private, so it's either this or reflection :(
object SbtLockfileCacheBuster {
  // Coursier caches resolutions, which breaks running freeze after update
  def clearCoursierUpdateCache(): Unit = {
    Tasks.resolutionsCache.clear()
    Tasks.reportsCache.clear()
  }
}
