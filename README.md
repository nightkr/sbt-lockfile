# sbt-lockfile

This is an attempt to rewrite the SBT plugin part of [Sbtix](https://gitlab.com/teozkr/Sbtix) to be independent
of Nix. For now, see [teozkr/Sbtix#41](https://gitlab.com/teozkr/Sbtix/issues/41) for more details.

## Usage

Add the plugin to your `project/plugins.sbt`:

```scala
resolvers += Resolver.sonatypeRepo("snapshots")
addSbtPlugin("se.nullable.sbtix" %% "sbt-lockfile" % "0.1-SNAPSHOT")

enablePlugins(LockfilePlugin)
```

Now it's possible to generate a `sbt.lock` which contains all of your build artifacts
and `sha256` checksums:

```
sbt freeze
sbt lockfileRequireFrozen
``` 

Whenever you regenerate the lockfile it will verify that all hashes match and adds newly
added artifacts.

## Local changes

Each commit on `master` is published as a SNAPSHOT automatically, but if you have made any local changes
then you can make other local projects use it by running the following:

```
sbt publishLocal
```

To revert, `rm ~/.ivy2/local/se.nullable.sbtix/sbt-lockfile`.

## Contact

Do you have any questions about this library or want to help out?

Feel free to [open an issue](https://gitlab.com/teozkr/sbt-lockfile/issues/new)
or join  `#sbtix` on freenode.
